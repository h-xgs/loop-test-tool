package com.android.looptesttool;

import android.app.KeyguardManager;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Log;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.Locale;

import static com.android.looptesttool.MainActivity.mainActivity;

public class Utils {

    /**
     * 日志总开关
     */
    public static boolean isDebug = false;
    /**
     * 把日志输出到缓存文件
     */
    public static boolean isOutputLog = false;

    /**
     * 浏览网页6分钟
     */
    // public static int WEB_TEST_TOTAL_TIME = 5;
    public static int WEB_TEST_TOTAL_TIME = 6 * 60;

    /**
     * 看本地视频1分钟
     */
    // public static int VIDEO_TEST_TOTAL_TIME = 5;
    public static int VIDEO_TEST_TOTAL_TIME = 60;

    /**
     * 休眠3分钟
     */
    // public static int OFFSCREEN_TEST_TOTAL_TIME = 5;
    public static int OFFSCREEN_TEST_TOTAL_TIME = 3 * 60;


    /**
     * 日志数目
     */
    private static int index = 0;

    /**
     * 计算时间
     *
     * @param seconds 秒数
     * @return 转换后的时间
     */
    public static String calculatePlayTime(int seconds) {
        int minute = seconds / 60;
        int second = seconds % 60;
        int hour = minute / 60;
        minute = minute % 60;
        if (hour > 0) {
            return String.format(Locale.getDefault(), "%02d:%02d:%02d", hour, minute, second);
        }
        return String.format(Locale.getDefault(), "%02d:%02d", minute, second);
    }

    public static void logE(String t, String m) {
        if (isDebug) {
            Log.e(t, m);
        }
        if (isOutputLog) {
            SharedPreferences.Editor editor = mainActivity.getSharedPreferences("mLog", Context.MODE_PRIVATE).edit();
            editor.putString(String.valueOf(index), "e：" + t + "：" + new Date() + m);
            editor.apply();
            index++;
        }
    }

    public static void logD(Context context, String t, String m) {
        if (isDebug) {
            Log.d(t, m);
        }
        if (isOutputLog) {
            SharedPreferences.Editor editor = context.getSharedPreferences("mLog", Context.MODE_PRIVATE).edit();
            editor.putString(String.valueOf(index), "d：" + t + "：" + new Date() + m);
            editor.apply();
            index++;
        }
    }

    public static void logW(Context context, String t, String m) {
        if (isDebug) {
            Log.w(t, m);
        }
        if (isOutputLog) {
            SharedPreferences.Editor editor = context.getSharedPreferences("mLog", Context.MODE_PRIVATE).edit();
            editor.putString(String.valueOf(index), "w：" + t + "：" + new Date() + m);
            editor.apply();
            index++;
        }
    }

    /**
     * 睡眠
     */
    public static void sleep(Context context) {
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);

        Class powerManagerClass = powerManager.getClass();
        try {
            Method goToSleepMethod = powerManagerClass.getDeclaredMethod("goToSleep", long.class);
            goToSleepMethod.invoke(powerManager, SystemClock.uptimeMillis());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 唤醒
     */
    public static void wakeUp(Context context) {
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);

        Class powerManagerClass = powerManager.getClass();
        try {
            Method wakeUpMethod = powerManagerClass.getDeclaredMethod("wakeUp", long.class);
            wakeUpMethod.invoke(powerManager, SystemClock.uptimeMillis());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
