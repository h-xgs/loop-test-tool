package com.android.looptesttool;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.os.PowerManager;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Date;

import static com.android.looptesttool.VideoActivity.videoActivity;
import static com.android.looptesttool.WebActivity.webActivity;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity测试";

    public static com.android.looptesttool.MainActivity mainActivity = null;

    public static final int MSG_UPDATE_WEB_TIME = 10001;
    public static final int MSG_UPDATE_VIDEO_TIME = 10002;

    public static boolean isTestIng = false;
    public static boolean startWebCountDown = false;
    public static boolean startVideoCountDown = false;

    public Button testButton;
    public EditText testNumberEdit;
    private MHandler mHandler;
    private Uri videoUri;
    private PowerManager powerManager = null;
    private AlarmManager alarmManager = null;
    private PendingIntent pi = null;

    public int testTotalNumber = 1;  // 循环测试总次数，默认1次
    public int currentTimes = 1;  // 当前次数
    private int webTotalTime = Utils.WEB_TEST_TOTAL_TIME;  // 浏览网页6分钟
    private int videoTotalTime = Utils.VIDEO_TEST_TOTAL_TIME;  // 看本地视频1分钟

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = this;
        setContentView(R.layout.activity_main);
        Utils.logD(this, TAG, "onCreate");

        testButton = findViewById(R.id.testButton);
        testNumberEdit = findViewById(R.id.testTime);
        mHandler = new MHandler(this);
        powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        initVideoPath();
        requestPermission();

        testButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 检查网络连接
                ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectionManager.getActiveNetworkInfo();
                if (!(networkInfo != null && networkInfo.isAvailable())) {
                    new AlertDialog.Builder(mainActivity)
                            .setTitle("请先连接WiFi才能开始测试！")
                            .setPositiveButton("确认", null)
                            .show();
                    return;
                }
                if (videoUri == null) {
                    Toast.makeText(mainActivity, "视频路径加载错误！", Toast.LENGTH_LONG).show();
                    initVideoPath();
                    return;
                }
                currentTimes = 1;
                String mNumber;
                mNumber = testNumberEdit.getText().toString().trim();
                // 默认循环1次
                if (mNumber.equals("")) {
                    mNumber = "1";
                    Toast.makeText(mainActivity, "未设置次数，已默认设置为1次", Toast.LENGTH_LONG).show();
                }
                try {
                    testTotalNumber = Integer.parseInt(mNumber);
                } catch (Exception e) {
                    e.printStackTrace();
                    testNumberEdit.setText("");
                    testTotalNumber = 1;
                    Toast.makeText(mainActivity, "次数太大！已设置为默认1次", Toast.LENGTH_LONG).show();
                    testNumberEdit.setText("");
                    return;
                }
                if (testTotalNumber < 1) {
                    testTotalNumber = 1;
                    Toast.makeText(mainActivity, "最少循环1次！默认已设置为1次", Toast.LENGTH_LONG).show();
                    testNumberEdit.setText("");
                    return;
                }
                if (testTotalNumber > 144) {
                    testTotalNumber = 1;
                    Toast.makeText(mainActivity, "次数太大，最大循环144次（24小时），请重新设置", Toast.LENGTH_LONG).show();
                    testNumberEdit.setText("");
                    return;
                }
                startTest();
                isTestIng = true;
                testButton.setEnabled(false);
                testNumberEdit.setEnabled(false);
                Utils.logW(mainActivity, TAG, new Date() + "：开始测试 ！");
            }
        });

        showLastTestResultDialog();
    }

    private void showLastTestResultDialog() {
        SharedPreferences sharedPreferences = getSharedPreferences("lastTestResult", Context.MODE_PRIVATE);
        if (sharedPreferences == null) {
            return;
        }
        String lr = sharedPreferences.getString("lastResult", "");
        boolean isShow = sharedPreferences.getBoolean("isShowLastResult", false);
        if (!lr.isEmpty() && isShow) {
            AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setTitle(lr)
                    .setPositiveButton("好的", null).create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    SharedPreferences.Editor editor = getSharedPreferences("lastTestResult", Context.MODE_PRIVATE).edit();
                    editor.putBoolean("isShowLastResult", false);
                    editor.apply();
                }
            });
            alertDialog.show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopAllTest();
    }

    public void stopAllTest() {
        isTestIng = false;
        startWebCountDown = false;
        startVideoCountDown = false;
        testNumberEdit.setText("");
        testButton.setEnabled(true);
        testNumberEdit.setEnabled(true);
        stopWeb();
        stopVideo();
        try {
            if (pi != null) {
                alarmManager.cancel(pi);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (powerManager.isIgnoringBatteryOptimizations(getPackageName())) {
                Toast.makeText(mainActivity, "权限已激活", Toast.LENGTH_SHORT).show();
            } else {
                AlertDialog mAlertDialog = new AlertDialog.Builder(this)
                        .setTitle("拒绝权限无法使用程序！")
                        .setPositiveButton("去授权", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermission();
                            }
                        })
                        .setNegativeButton("退出APP", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).create();
                // 点击弹窗外区域不消失
                mAlertDialog.setCanceledOnTouchOutside(false);
                mAlertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        return true;
                    }
                });
                mAlertDialog.show();
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (isTestIng) {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                new AlertDialog.Builder(this)
                        .setTitle("是否确定退出测试？")
                        .setPositiveButton("确认退出", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                stopAllTest();
                                showResultDialog("退出测试，测试已成功完成:" + (currentTimes - 1) + "轮，总次数:" + testTotalNumber);
                            }
                        })
                        .setNegativeButton("取消", null)
                        .show();
                return true;
            }
            if (keyCode == KeyEvent.KEYCODE_HOME || keyCode == KeyEvent.KEYCODE_MENU) {
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public void showResultDialog(String text) {
        new AlertDialog.Builder(this)
                .setTitle(text)
                .setPositiveButton("确认", null)
                .show();
    }

    /**
     * 加载视频
     */
    private void initVideoPath() {
        String uri = "android.resource://" + getPackageName() + "/" + R.raw.knc_simplelife;
        videoUri = Uri.parse(uri);
        Utils.logW(mainActivity, TAG, "onActivityResult: " + videoUri);
    }

    /**
     * 申请权限
     */
    private void requestPermission() {
        //添加底电耗模式和待机模式的白名单
        if (!powerManager.isIgnoringBatteryOptimizations(getPackageName())) {
            Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
            intent.setData(Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, 1);
        }
    }

    private void updateWebTestTime() {
        if (startWebCountDown) {
            webTotalTime -= 1;
            if (webTotalTime > 0) {
                Utils.logW(mainActivity, TAG, "updateWebTestTime: 剩余时间：" + Utils.calculatePlayTime(webTotalTime));
            } else {
                startWebCountDown = false;
                stopWeb();
                playVideo();
                Utils.logW(mainActivity, TAG, new Date() + "：网页浏览完成！开始播放视频");
            }
        }
    }

    private void updateVideoTestTime() {
        if (startVideoCountDown) {
            videoTotalTime -= 1;
            if (videoTotalTime > 0) {
                Utils.logW(mainActivity, TAG, "updateVideoTestTime: 剩余时间：" + Utils.calculatePlayTime(videoTotalTime));
            } else {
                startVideoCountDown = false;
                stopVideo();
                offScreen();
                Utils.logW(mainActivity, TAG, new Date() + "：视频播放完成！开始休眠");
            }
        }
    }

    /**
     * 开启一轮测试
     */
    public void startTest() {
        SharedPreferences.Editor editor = getSharedPreferences("lastTestResult", Context.MODE_PRIVATE).edit();
        editor.putString("lastResult", "最后一次测试结果：总循环次数:" + mainActivity.testTotalNumber + " 次，已成功完成:" + (mainActivity.currentTimes - 1) + " 次");
        editor.putBoolean("isShowLastResult", true);
        editor.apply();
        webTotalTime = Utils.WEB_TEST_TOTAL_TIME;
        videoTotalTime = Utils.VIDEO_TEST_TOTAL_TIME;
        openWeb();
    }

    /**
     * 浏览网页
     */
    private void openWeb() {
        // 开始倒计时
        startWebCountDown = true;
        // 2秒钟之后开启网页浏览计时
        mHandler.sendEmptyMessageDelayed(MSG_UPDATE_WEB_TIME, 2000);
        Intent webIt = new Intent(mainActivity, WebActivity.class);
        try {
            startActivity(webIt);
            Utils.logW(mainActivity, TAG, "openWeb startWebActivity");
        } catch (Exception e) {
            e.printStackTrace();
            Utils.logE(TAG, e.getMessage());
        }
    }

    /**
     * 停止浏览网页
     */
    private void stopWeb() {
        try {
            if (webActivity != null) {
                webActivity.finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Utils.logE(TAG, e.getMessage());
        }
    }

    /**
     * 播放视频
     */
    private void playVideo() {
        startVideoCountDown = true;
        mHandler.sendEmptyMessageDelayed(MSG_UPDATE_VIDEO_TIME, 2000);
        Intent videoIt = new Intent(mainActivity, VideoActivity.class);
        videoIt.putExtra("path", videoUri);
        videoIt.putExtra("loop", true);
        try {
            startActivity(videoIt);
            Utils.logW(mainActivity, TAG, "playVideo startVideoActivity");
        } catch (Exception e) {
            e.printStackTrace();
            Utils.logE(TAG, e.getMessage());
        }
    }

    /**
     * 停止播放视频
     */
    private void stopVideo() {
        try {
            if (videoActivity != null) {
                videoActivity.finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Utils.logE(TAG, e.getMessage());
        }
    }

    /**
     * 锁屏休眠
     */
    private void offScreen() {
        // 创建定时亮屏任务
        Utils.logW(mainActivity, TAG, new Date() + "创建亮屏定时任务");
        long triggerAtTime = System.currentTimeMillis() + Utils.OFFSCREEN_TEST_TOTAL_TIME * 1000L;
        Intent i = new Intent(this, OnScreenService.class);
        pi = PendingIntent.getService(this, 0, i, PendingIntent.FLAG_IMMUTABLE);
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, triggerAtTime, pi);
        Utils.sleep(mainActivity);
    }


    class MHandler extends StaticHandler<MainActivity> {

        public MHandler(MainActivity mActivity) {
            super(mActivity);
        }

        @Override
        protected void handleMessage(Message msg, MainActivity mActivity) {
            super.handleMessage(msg, mActivity);
            switch (msg.what) {
                case MSG_UPDATE_WEB_TIME:
                    updateWebTestTime();
                    if (startWebCountDown) {
                        // 每隔1秒执行一次
                        mHandler.sendEmptyMessageDelayed(MSG_UPDATE_WEB_TIME, 1000);
                    }
                    break;
                case MSG_UPDATE_VIDEO_TIME:
                    updateVideoTestTime();
                    if (startVideoCountDown) {
                        // 每隔1秒执行一次
                        mHandler.sendEmptyMessageDelayed(MSG_UPDATE_VIDEO_TIME, 1000);
                    }
                    break;
                default:
                    break;
            }
        }
    }

}