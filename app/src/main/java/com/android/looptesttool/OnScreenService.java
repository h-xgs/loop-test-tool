package com.android.looptesttool;

import static com.android.looptesttool.MainActivity.mainActivity;

import android.app.KeyguardManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import java.util.Date;

public class OnScreenService extends Service {

    private static final String TAG = "OnScreenService测试";

    @Override
    public void onCreate() {
        super.onCreate();
        Utils.logD(this, TAG, "onCreate executed");
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Utils.logD(this, TAG, "onStartCommand executed");
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        if (keyguardManager != null) {
            try {
                keyguardManager.requestDismissKeyguard(mainActivity, null);
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(TAG, e.getMessage());
            }
        }
        Utils.wakeUp(this);
        if (mainActivity != null) {
            mainActivity.currentTimes += 1;
            if (mainActivity.currentTimes <= mainActivity.testTotalNumber) {
                mainActivity.startTest();
                Utils.logW(mainActivity, TAG, new Date() + "：休眠完成！进入第 " + mainActivity.currentTimes + " 轮测试");
            } else {
                MainActivity.isTestIng = false;
                mainActivity.testButton.setEnabled(true);
                mainActivity.testNumberEdit.setEnabled(true);
                mainActivity.testNumberEdit.setText("");
                mainActivity.showResultDialog(mainActivity.testTotalNumber + " 次测试已全部完成！");
                SharedPreferences.Editor editor = getSharedPreferences("lastTestResult", Context.MODE_PRIVATE).edit();
                editor.putString("lastResult", "最后一次测试结果：" + mainActivity.testTotalNumber + " 次测试已全部完成！");
                editor.putBoolean("isShowLastResult", false);
                editor.apply();
                Utils.logW(mainActivity, TAG, new Date() + "：测试全部完成！");
            }
        }
        stopSelf();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.logD(this, TAG, "onDestroy executed");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}