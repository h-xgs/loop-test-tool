package com.android.looptesttool;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.util.LogWriter;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class WebActivity extends BaseActivity {

    public static com.android.looptesttool.WebActivity webActivity = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        webActivity = this;
        // 设置屏幕不休眠
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        hookWebView();
        setContentView(initView());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        webActivity = null;
    }

    private View initView() {
        String url = "https://www.baidu.com";
        final WebView webView = new WebView(this);
        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        return webView;
    }

    @SuppressLint("SoonBlockedPrivateApi")
    private void hookWebView() {
        try {
            Class factoryClass = Class.forName("android.webkit.WebViewFactory");
            Field field = factoryClass.getDeclaredField("sProviderInstance");
            field.setAccessible(true);
            Object sProviderInstance = field.get(null);
            if (sProviderInstance != null) {
                Log.i("ww", "sProviderInstance isn't null");
                return;
            }
            Method getProviderClassMethod;
            getProviderClassMethod = factoryClass.getDeclaredMethod("getProviderClass");
            getProviderClassMethod.setAccessible(true);
            Class factoryProviderClass = (Class) getProviderClassMethod.invoke(factoryClass);
            Class delegateClass = Class.forName("android.webkit.WebViewDelegate");
            Constructor delegateConstructor = delegateClass.getDeclaredConstructor();
            delegateConstructor.setAccessible(true);
            Field chromiumMethodName = factoryClass.getDeclaredField("CHROMIUM_WEBVIEW_FACTORY_METHOD");
            chromiumMethodName.setAccessible(true);
            String chromiumMethodNameStr = (String) chromiumMethodName.get(null);
            if (chromiumMethodNameStr == null) {
                chromiumMethodNameStr = "create";
            }
            Method staticFactory = factoryProviderClass.getMethod(chromiumMethodNameStr, delegateClass);
            if (staticFactory != null) {
                sProviderInstance = staticFactory.invoke(null, delegateConstructor.newInstance());
            }
            if (sProviderInstance != null) {
                field.set("sProviderInstance", sProviderInstance);
                Log.i("ww", "Hook success!");
            } else {
                Log.i("ww", "Hook failed!");
            }
        } catch (
                Throwable e) {
            Log.w("ww", e);
        }
    }
}
