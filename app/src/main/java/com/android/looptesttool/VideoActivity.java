package com.android.looptesttool;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.view.WindowManager;
import android.widget.VideoView;

public class VideoActivity extends BaseActivity {

    private static final int MSG_SHOW_PREVIEW = 4001;
    public static com.android.looptesttool.VideoActivity videoActivity = null;

    VideoView videoView;
    Uri videoUri;
    VideoHandler videoHandler;
    boolean isLoop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        videoActivity = this;
        setContentView(R.layout.activity_video);
        // 设置屏幕不休眠
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        videoView = findViewById(R.id.videoView);
        videoHandler = new VideoHandler(this);
        try {
            videoUri = getIntent().getParcelableExtra("path");
            videoView.setVideoURI(videoUri);
            showPreview();
            isLoop = getIntent().getBooleanExtra("loop", false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //播放器准备就绪
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                videoView.setBackground(null);
            }
        });
        // 在播放完毕被回调
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (isLoop) {
                    play();
                }
            }
        });
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                // "播放出错"
                return false;
            }
        });
        play();
    }

    private void play() {
        videoView.setBackground(null);
        videoView.start();
    }

    private void showPreview() {
        Bitmap firstFrame = getFirstFrameDrawable(videoUri);
        if (firstFrame != null) {
            BitmapDrawable bd = new BitmapDrawable(getResources(), firstFrame);
            videoView.setBackground(bd);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (videoView.isPlaying()) {
            videoView.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!videoView.isPlaying()) {
            videoView.start();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        videoHandler.removeCallbacksAndMessages(null);
        videoView.suspend();
        videoActivity = null;
    }

    protected Bitmap getFirstFrameDrawable(Uri uri) {
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        try {
            mmr.setDataSource(this, uri);
            return mmr.getFrameAtTime();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    class VideoHandler extends StaticHandler<com.android.looptesttool.VideoActivity> {

        public VideoHandler(com.android.looptesttool.VideoActivity videoActivity) {
            super(videoActivity);
        }

        @Override
        protected void handleMessage(Message msg, com.android.looptesttool.VideoActivity videoActivity) {
            super.handleMessage(msg, videoActivity);
            if (msg.what == MSG_SHOW_PREVIEW) {
                showPreview();
            }
        }
    }

}