package com.android.looptesttool;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.KeyEvent;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import static com.android.looptesttool.MainActivity.mainActivity;

public class BaseActivity extends AppCompatActivity {

    public String TAG = getClass().getSimpleName() + "测试";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.logD(this, TAG, "onCreate");
        String tips = "当前循环次数:" + mainActivity.currentTimes + "  /  总次数:" + mainActivity.testTotalNumber;
        try {
            getSupportActionBar().setTitle(tips);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Utils.logD(this, TAG, "onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.logD(this, TAG, "onResume");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utils.logD(this, TAG, "onDestroy");
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            new AlertDialog.Builder(this)
                    .setTitle("是否确定退出测试？")
                    .setPositiveButton("确认退出", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            mainActivity.showResultDialog("退出测试，测试已成功完成:" + (mainActivity.currentTimes - 1) + "轮，总次数:" + mainActivity.testTotalNumber);
                            mainActivity.stopAllTest();
                        }
                    })
                    .setNegativeButton("取消", null)
                    .show();
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_HOME || keyCode == KeyEvent.KEYCODE_MENU) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
